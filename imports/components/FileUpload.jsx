import React, { Component } from 'react'
export default class FileUpload extends Component {
    constructor(props){
        super(props)
        this.state = {
            uploading:[],
            progress:0,
            inProgress:false
        }
    }
    uploadIt =(e)=>{
        e.preventDefault()
        //console.log(this)
        let self = this
        const data = {file:e.currentTarget.files,self:self}
        this.props.changeFileInput(data)
    }
    showUploads(){

    }
    render() {
        const {accept} = this.props
        return (
            <div>
                <input type="file" id="fileinput" ref="fileinput" onChange={this.uploadIt} accept={accept}/>
            </div>
        )
    }

}
