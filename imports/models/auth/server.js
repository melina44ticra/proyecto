Meteor.methods({
    userCreate:function(form){
        //return 'hola soy el servidor'
        if(form.password != form.re_password)
            throw new Meteor.Error(406, "la contraseña debe ser igual a la confirmacion de contraseña");
        Accounts.createUser({
            createdAt:new Date(),
            email:form.email,
            username:form.username,
            password:form.password,
            profile:{
                fullname:form.namecomplete
            }
        })

        return 'usuario creado correctamente'
    }
})