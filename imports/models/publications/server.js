import {publicationsClass} from './class'
import {categoryClass} from '../category/class'
import UserFiles from '../files/class'
import {queryPublications} from './querys'

publicationsClass.extend({
    meteorMethods:{
        newPublication(form){
            try {
                const file = UserFiles.findOne({_id:form.image})
                const URL = file.link()
                const indextext = file.mime.lastIndexOf('/')
                var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                this.title = form.title
                this.description = form.description
                this.nameCategory = category.name
                this.idCategory = category._id
                this.idfile = form.image
                this.urlfile = newUrl
                this.startDate = form.startDate
                this.user = Meteor.userId()
                this.username = Meteor.user().username
                this.created_view = new Date()
                this.typepub = file.mime.slice(0,indextext)
                this.save()
                return "Guardado Correctamente"
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
            /*this.title = form.title
            this.description = form.description
            this.startDate = form.startDate*/
            //this.save() 
        },
        editPublication(form){
            try {
                if(form.image){
                    const URL = UserFiles.findOne({_id:form.image}).link()
                    var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title = form.title
                    this.description = form.description
                    this.nameCategory = category.name
                    this.idCategory = category._id
                    this.idfile = form.image
                    this.urlfile = newUrl
                    this.startDate = form.startDate
                    this.user = Meteor.userId()
                    this.username = Meteor.user().username
                    this.created_view = new Date()
                    this.save()
                    return "Imagen editada correctamente"
                }else{
                        console.log(form.price)
                        //const URL = UserFiles.findOne({_id:form.image}).link()
                        //var newUrl = URL.replace(/^.*\/\/[^\/]+/,'')
                        const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                        this.title = form.title
                        this.description = form.description
                        this.nameCategory = category.name
                        this.idCategory = category._id
                        //this.idfile = form.image
                        //this.urlfile = newUrl
                        this.startDate = form.startDate
                        this.user = Meteor.userId()
                        this.username = Meteor.user().username
                        this.created_view = new Date()
                        this.save()
                        return "editado Correctamente SIN imagen"
                }
            } catch (error) {
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }           
        },
        updateStatePublication(){
            try{
                this.active = this.active?false:true
                return this.save()
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        }
    }
})

Meteor.publish('publications',function(options,type){
    try {
        const subs = new queryPublications(options,this)
        return subs[type]()
    } catch (error) {
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})

