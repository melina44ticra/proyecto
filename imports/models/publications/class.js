import { Class } from 'meteor/jagi:astronomy'; // importar la libreria
const publicationsCollection = new Mongo.Collection('publications',{idGeneration:'MONGO'});//vamos  a crear nuestra collections o tabla en mongodb

export const publicationsClass = Class.create({
  name: 'publicationsClass',
  collection: publicationsCollection,
  fields: {
    title:{
        type:String,
        //default:'holitas',
        //optional: true 
    },
    description:{
        type:String,
    },
    startDate:{
        type:Date
    },
    nameCategory:{
        type:String
    },
    idfile:{
        type:String
    },
    urlfile:{
        type:String
    },
    active:{
        type:Boolean,
        default:true
    },
    idCategory:{
      type:Mongo.ObjectID
    },
    typepub:{
      type:String,
      default:'image'
    },
    username: String,
    created_view:{
      type:Date,
    }
  }
});

if (Meteor.isServer) {
    publicationsClass.extend({
      fields: {
        user: String,
        
      },
      behaviors: {
        timestamp: {
          hasCreatedField: true,
          createdFieldName: 'createdAt',
          hasUpdatedField: true,
          updatedFieldName: 'updatedAt'
        }
      }
    });
}

