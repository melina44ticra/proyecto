import {categoryClass} from './class'
import {queryCategorys} from './querys'

categoryClass.extend({
    meteorMethods:{
        createNewCategory(form){
            this.set(form)
            this.user = Meteor.userId()
            this.save()
            return 'Creado con exito'
        }
    }
});

Meteor.publish('category',function(options,type){
    try {
        const subs = new queryCategorys(options,this)
        return subs[type]()
    } catch (error) {
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})
