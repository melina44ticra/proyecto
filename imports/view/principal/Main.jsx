import React, { Component } from 'react'
import { Link,Switch } from 'react-router-dom'
import {publicationsClass} from '../../models/publications/class'
import { withTracker } from 'meteor/react-meteor-data'
import {categoryClass} from '../../models/category/class'

const categorySelect = new ReactiveVar(undefined)
class Main extends Component {
    selectCategory = (category) => {
        categorySelect.set(category?category:undefined)
    }
    render() {
        const {publications,subscriptionPublications,categorys,publicationsLimitTwo} = this.props

        return (
            <div>

                <section className="hero">
                    <div className="container">
                        <div className="hero__slider owl-carousel">
                            <div className="hero__items set-bg" data-setbg="/principal/img/portada_a.jpg">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <div className="label">Adventure</div>
                                            <h2>WALL - E</h2>
                                            <p>Luego de pasar años limpiando la Tierra desierta, el robot Wall-E ...</p>
                                            <a href="#"><span>Ver Ahora</span> <i className="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="hero__items set-bg" data-setbg="/principal/img/portada_b.jpg">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <div className="label">Adventure</div>
                                            <h2>Avengers END GAME</h2>
                                            <p>Los Vengadores restantes deben encontrar una manera ...</p>
                                            <a href="#"><span>Ver Ahora</span> <i className="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="hero__items set-bg" data-setbg="/principal/img/portada_c.jpg">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="hero__text">
                                            <div className="label">Horror</div>
                                            <h2>IT</h2>
                                            <p>Varios niños de una pequeña ciudad del estado de Maine se alían...</p>
                                            <a href="#"><span>Ver Ahora</span> <i className="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="product spad">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="trending__product">
                                    <div className="row">
                                        <div className="col-lg-8 col-md-8 col-sm-8">
                                            <div className="section-title">
                                                <h4>pelicuas publicadas ahora</h4>
                                            </div>
                                        </div>
                                        <div className="col-lg-4 col-md-4 col-sm-4">
                                            <div className="btn__all">
                                                <a href="#" className="primary-btn">Ver todo <span
                                                        className="arrow_right"></span></a>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="row">
                                        {publications.map((data,key)=>{
                                            return <div key={`article_${key}`} className="col-lg-4 col-md-6 col-sm-6">
                                            <div className="product__item">
                                                <div className="product__item__pic set-bg ">
                                                {data.typepub == 'video'? 
                                                    (
                                                        <video className="product__item__pic set-bg "  controls>
                                                            <source src={data.urlfile}/>
                                                        </video>
                                                    )
                                                        :
                                                       <img className="product__item__pic set-bg " src={data.urlfile}/>
                                                }

                                                    
                                                    <div className="ep">{moment(data.created_view).format('MMM')}</div>
                                                    <div className="comment"><i className="fa fa-comments"></i> 11</div>
                                                    <div className="view"><i className="fa fa-eye"></i> 9141</div>
                                                </div>
                                                <div className="product__item__text">
                                                    <ul>
                                                        <li>Activo</li>
                                                        <li>Pelicula</li>
                                                    </ul>
                                                    <h5><a href="#">{data.title}</a></h5>
                                                </div>
                                            </div>
                                        </div>
                                        })
                                    }
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-8">
                                <div className="product__sidebar">
                                    <div className="product__sidebar__view">
                                        <div className="section-title">
                                            <h5>Top de Categorias</h5>
                                        </div>
                                        <li>
                                            <a style={{cursor:'pointer'}}  className="section-title" onClick={(e)=>{this.selectCategory(null)}}>
                                                <p>Todas las Categorias</p>
                                            </a>
                                        </li>

                                        {
                                            categorys.map((data,key)=>{
                                                return <li key={`article_${key}`}>
                                                    <a style={{cursor:'pointer'}} className="product__sidebar__view__item set-bg mix day years" onClick={()=>{this.selectCategory(data)}}>
                                                        <p>{data.name}</p>
                                                    </a>
                                                </li>
                                            })

                                        }


                                    </div>
                                    <div className="product__sidebar__comment">
                                        <div className="section-title">
                                            <h5>Resientemente agregados</h5>
                                        </div>
                                        {
                                            publicationsLimitTwo.map((data,key)=>{
                                                return <div key={`article_${key}`} className="media post_item">
                                                            
                                                            {data.typepub == 'video'? 
                                                                (
                                                                    <img src='/principal/img/VIDEOICON.png'style={{width:'20%'}}/>
                                                                )
                                                                :
                                                                <img src={data.urlfile} style={{width:'35%'}}/>
                                                            }
                                                            <div className="product__sidebar__comment__item">
                                                                <div className="product__sidebar__comment__item__pic">
                                                                    <img alt="" />
                                                                </div>
                                                                <div className="product__sidebar__comment__item__text">
                                                                    <ul>
                                                                        <li>{moment(data.created_view).format('MMMM DD, YYYY')}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                            })
                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}
export default withTracker((props)=>{
    const optiospublications = {
        category : categorySelect.get()
    }
    const subscriptionPublications = Meteor.subscribe('publications',optiospublications,'getAllPublications')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const publications = publicationsClass.find({},{sort:{createdAt:-1}}).fetch()
    const publicationsLimitTwo = publicationsClass.find({},{sort:{createdAt:-1},limit:2}).fetch()
    return {publications,subscriptionPublications,categorys,subcriptionCategory,publicationsLimitTwo}
})(Main)

