import React, { Component } from "react"
import {Link, Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'


export default class Principal extends Component{
    constructor (props){
        super(props)
        this.state={
            loader:true
        }
    }
    componentDidMount(){
        import '/imports/assets/principal/css'
        import '/imports/assets/principal/js'
        import desingPrincipal from '/imports/assets/principal/js/main'
        const mthis =this
        desingPrincipal()
        setTimeout(function(){
            mthis.setState({loader:false})
            desingPrincipal()
            $("#preloder-active").delay(200).fadeOut("slow");
        },5000)
        
    }
    render(){
        const {routes} = this.props
        return(
            <div>
                {this.state.loader?
                <div id="preloder">
                    <div className="loader"></div>
                </div>
                :
                <div>
                    <header className="header">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-2">
                                    <div className="header__logo">
                                        <a href="/">
                                            <img src="/principal/img/logo.png" width="93 px" height="50 px" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div className="col-lg-8">
                                    <div className="header__nav">
                                        <nav className="header__menu mobile-menu">
                                            <ul>
                                                <li className="active"><a href="/">Principal</a></li>
                                                <li><a href="/">Categorias <span
                                                            className="arrow_carrot-down"></span></a>
                                                    <ul className="dropdown">
                                                        <li><a href="/">Categorias</a></li>
                                                        <li>
                                                            <Link to="/bienvenido/registro">Registrarme</Link>
                                                        </li>
                                                        <li>
                                                            <Link to="/bienvenido/login">Iniciar Sesion</Link>
                                                        </li>
                                                    </ul>
                                                </li>
                                                
                                                <li><a href="#">Contactanos</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div className="col-lg-2">
                                    <div className="header__right">
                                        <a href="#" className="search-switch"><span className="icon_search"></span></a>
                                        <a href="/login/"><span className="icon_profile"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div id="mobile-menu-wrap"></div>
                        </div>
                    </header>
                    <main>
                        <Switch>
                            {/*
                            <Route exact path="/" component={Principal} />*/}
                            {
                            routes.map((route,i)=>{
                                return <SwitchRoutes key={i} {...route} />
                            })
                            }
                        </Switch>

                    </main>


                    <footer className="footer">
                        <div className="page-up">
                            <a href="#" id="scrollToTopButton"><span className="arrow_carrot-up"></span></a>
                        </div>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3">
                                    <div className="footer__logo">
                                        <a href="/"><img src="/principal/img/logo.png" alt="" /></a>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="footer__nav">
                                        <ul>
                                            <li className="active"><a href="/">Principal</a></li>
                                            <li><a href="/">Categorias</a></li>
                                            
                                            <li><a href="#">Contactanos</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-3">
                                    <p>
                                        Copyright &copy;Todos los derechos rservados | Cartelera <i
                                            className="fa fa-heart" aria-hidden="true"></i> by <a
                                            href="https://colorlib.com" target="_blank">Colorlib</a>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </footer>

                    <div className="search-model">
                        <div className="h-100 d-flex align-items-center justify-content-center">
                            <div className="search-close-switch"><i className="icon_close"></i></div>
                            <div>
                                <form className="search-model-form">
                                    <input type="text" id="search-input" placeholder="Search here....." /> </form>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </div>
        )
    }
}