import React, { Component } from 'react'
import {publicationsClass} from '../../models/publications/class' 
import {categoryClass} from '../../models/category/class'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import DatePicker from "react-datepicker";
import FileUpload from '../../components/FileUpload'
import uploadFiles from '../../utils/upload'


class EditPublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:props.getpublication.title,
                description:props.getpublication.description,
                startDate:props.getpublication.startDate,
                image:null,
                category:props.getpublication.idCategory._str
            },
            errors:{}
        }
    }
    validateForm=()=>{
        const {form} = this.state
        let errorsform = {}
        let formsIsValid = true
        if(!form.title){ 
            formsIsValid = false
            errorsform['title'] = "El titulo no puede estar vacio"
        }
        if(!form.description){ 
            formsIsValid = false
            errorsform['description'] = "la description no puede estar vacia"
        }
        if(!form.startDate){ 
            formsIsValid = false
            errorsform['startDate'] = "la fecha no puede estar vacio"
        }
        this.setState({errors:errorsform})
        return formsIsValid
    }

    editPublications=(e)=>{
        e.preventDefault()
        const {history} = this.props
        if(this.validateForm()){
            //alert('enviando formulario')
            const editpublicacion = this.props.getpublication    
            const {form} = this.state
            
            if(this.state.form.image){         
                const {form:{image}} = this.state
                const uf = new uploadFiles(image.file,image.self) 
                uf.newUpload(
                    function(error,success){
                    if(error){
                        console.log('--------------****-------------')
                        console.log(error)
                        console.log('--------------****-------------')
                    }else{
                            form.image = success._id
                            editpublicacion.callMethod('editPublication',form,(error,result)=>{
                                if(error){
                                    alert(error)
                                }else{
                                    alert(result)
                                    history.push('/dashboard/user-publications')
                                }
                            })       
                    }
                    }
                )
            }else{
                editpublicacion.callMethod('editPublication',form,(error,result)=>{
                    if(error){
                        alert(error)
                    }else{
                        alert(result)
                        history.push('/dashboard/user-publications')
                    }
                })  
            }
            return false
            
        }else{
           alert('el formulario tiene errores')
        }
    }
    changeTextInput = (e) =>{
        const value = e.target.value
        const property = e.target.name
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [property]:value,
                }
            }
        ))

    }
    changeDateInput = (type,date) =>{
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    [type]:date,
                }
            }
        ))
    }
    changeSelectInput =(e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    category:value,
                }
            }
        ))
    }
    changeFileInput =(data)=>{
        //console.log(data)
        const inputfile = data.file
        if(inputfile && inputfile[0]){
            console.log(inputfile[0])
            let reader = new FileReader()
            reader.onload = function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputfile[0])

            this.setState(prevState=>(
                {form:{
                        ...prevState.form,
                        image:data,
                    }
                }
            ))
        }
    }
    render() {
        const {errors,form} = this.state
        const {categorys,subcriptionCategory,getpublication,subcriptionPublication}= this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Editar Publicacion</h4>
                                    </div>
                                    {!subcriptionCategory.ready() && subcriptionPublication.ready()?
                                        <h1>Cargando!!!</h1>
                                    :
                                        <div className="card-body">
                                            <form onSubmit={this.editPublications} id="editPublication">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label>Titulo de publicacion</label>
                                                            <input type="text" className={errors.title?"form-control is-invalid":"form-control"} value={form.title} name={'title'} onChange={this.changeTextInput} autoComplete="off"/>
                                                            {errors.title?<div className="invalid-feedback">{errors.title}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Descripcion</label>
                                                            <textarea type="text" value={form.description}  className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"}  name={'description'} onChange={this.changeTextInput}>
                                                            </textarea>
                                                            {errors.description?<div className="invalid-feedback">{errors.description}</div>:null}
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Seleccione una Categoria</label>
                                                            <select className="form-control" value={form.category} onChange={this.changeSelectInput}>
                                                                <option>Seleccione una categoria</option>  
                                                                {
                                                                    categorys.map((category,key)=>{
                                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                                    })
                                                                }
                                                            </select>
                                                        </div>
                                                        <div className="form-group">
                                                            <label className="form-label">Archivo</label>
                                                            {/*<input type="file" className='form-contro'  name={'file'} onChange={this.changeFileInput}/>*/}
                                                            <FileUpload changeFileInput={this.changeFileInput}/>
                                                        </div>
                                                        <div className="form-group">
                                                            <label>Fecha de Publicacion</label>
                                                            {/*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.changeTextInput} autoComplete="off"/>*/}
                                                            <div>
                                                                <DatePicker selected={form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} name={'startDate'} 
                                                                    onChange={date => {
                                                                        this.changeDateInput('startDate',date)
                                                                    }}
                                                                />
                                                            </div>
                                                            {errors.startDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.startDate}</div>:null}
                                                        </div>
                                            
                                                    </div>
                                                    <div className="col-md-6 d-flex justify-content-center">
                                                        <div className="author-box-center">
                                                            <img alt="image" src={getpublication.urlfile}  id="previewimage" className="rounded-circle author-box-picture" style={{width: '100%',height:'500'}} />
                                                            <div className="clearfix" />
                                                            <div className="author-box-name">
                                                                
                                                            </div>
                                                            <div className="author-box-job d-flex justify-content-center">Vista Previa</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i> Aplicar Edicion</button>
                                            </form>
                                        </div>
                                    }
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const {location:{state:{publication}}} = props
    const subcriptionPublication = Meteor.subscribe('publications',{pub:publication},'getOnePublication')
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const getpublication = publicationsClass.findOne()
    return {categorys,subcriptionCategory,getpublication,subcriptionPublication}
})(EditPublication)
