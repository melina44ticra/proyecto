import React, { Component } from 'react'
import {publicationsClass} from '../../models/publications/class'
import {categoryClass} from '../../models/category/class'
import DatePicker from "react-datepicker";
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import FileUpload from '../../components/FileUpload';
import uploadFiles from '../../utils/upload'

class CreatePublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:null,
                description:null,
                startDate:new Date(),
                image:null,
                category:null

            },
            typefile:'image',
            videouri:null,
            errors:{}

        }
    }
    validateForm=()=>{
        const {form} = this.state
        let errorsform = {}
        let formsIsValid=true
        if(!form.title){
            formsIsValid=false
            errorsform['title']="el titulo no puede estar vacio"
        }
        if(!form.description){
            formsIsValid=false
            errorsform['description']="la descripcion no puede estar vacio"
        }
        if(!form.startDate){
            formsIsValid=false
            errorsform['startDate']="la fecha no puede estar vacio"
        }
        this.setState({errors:errorsform})
        return formsIsValid
    }
    createNewPublications=(e)=>{ 
        e.preventDefault()
        if(this.validateForm()){
            //alert('enviando formulario')
            //console.log(this.state.form)
            const newpublicacion = new publicationsClass()
            const {form} = this.state
            
            const {form:{image}} = this.state
            const uf = new uploadFiles(image.file,image.self) 
            const mthis=this
            uf.newUpload(
                function(error,success){
                   if(error){
                    console.log('---------------------------------')
                    console.log(error)
                    console.log('---------------------------------')
                   }else{
                        form.image = success._id
                        newpublicacion.callMethod('newPublication',form,(error,result)=>{ 
                            if(error){
                                alert(error)
                            }else{
                                alert(result)
                                document.getElementById("newPublication").reset()
                                mthis.setState({typefile:'image'})
                            }

                        })       
                   }
                }
            )
        
        }else{
           alert('el formulario tiene errores')
        }
    }

    changeTextInput=(e)=>{
        const value=e.target.value  
        const property=e.target.name 
        this.setState(prevState=>(
            {form:{
                ...prevState.form,
                [property]:value,
            }
        }
    ))

    }
    changeDateInput =(type,date) =>{
        this.setState(prevState=>(
            {form:{
                ...prevState.form,
                [type]:date,
            }
        }
    ))
    }
    changeSelectInput =(e)=>{
        const value = e.target.value
        this.setState(prevState=>(
            {form:{
                    ...prevState.form,
                    category:value,
                }
            }
        ))
    }

    changeFileInput =(data)=>{
        //console.log(data)
        const inputfile = data.file
        if(inputfile && inputfile[0]){
            console.log(inputfile[0])
            const indextext = inputfile[0].type.lastIndexOf('/')
            const typefile = inputfile[0].type.slice(0,indextext)
            if(typefile == 'image'){
                this.setState({typefile:'image'})
                let reader = new FileReader()
                reader.onload = function(v){
                    $('#previewimage').attr('src',v.target.result)
                }
                reader.readAsDataURL(inputfile[0])
    
                
            }else{
                
                let file = inputfile[0]
                let blobURL = URL.createObjectURL(file)
                console.log(blobURL)
                this.setState({videouri:blobURL})
                this.setState({typefile:'video'})
                //$('#previewvideo').src = blobURL
            }
            this.setState(prevState=>(
                {form:{
                        ...prevState.form,
                        image:data,
                    }
                }
            ))
            
        }
    }


    render() {
        const {errors,typefile,videouri} = this.state
        const {categorys,subcriptionCategory} = this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Crear nueva publicacion</h4>
                                    </div>
                                    {!subcriptionCategory.ready()?
                                        <h1>Cargando!!!</h1>
                                    :

                                    <div className="card-body">
                                        <form onSubmit={this.createNewPublications} id="newPublication">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Titulo de publicacion</label>
                                                        <input type="text" className={errors.title?"form-control is-invalid":"form-control"}name={'title'} onChange={this.changeTextInput}/>
                                                        {errors.title?<div className="invalid-feedback">{errors.title}</div> : null}
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Descricpion</label>
                                                        <input type="text"className={errors.description?"form-control invoice-input is-invalid":"form-control invoice-input"}name={'description'} onChange={this.changeTextInput}/>
                                                        {errors.description?<div className="invalid-feedback">{errors.description}</div> : null}
                                                    </div>
                                                    <div className="form-group">
                                                            <label>Seleccione la Categoria</label>
                                                            <select className="form-control" onChange={this.changeSelectInput}>
                                                                <option defaultValue>Seleccione una categoria</option>  
                                                                {
                                                                    categorys.map((category,key)=>{
                                                                        return <option key={`category ${key}`} value={category._id}>{category.name}</option>
                                                                    })
                                                                }
                                                            </select>
                                                    </div>
                                                    <div className="form-group">
                                                            <label className="form-label">Archivo</label>
                                                            {/*<input type="file" className='form-contro'  name={'file'} onChange={this.changeFileInput}/>*/}
                                                            <FileUpload changeFileInput={this.changeFileInput} accept=".jpeg, .jpg, .png, .mp4"/>
                                                    </div>

                                                    <div className="form-group">
                                                        <label>Fecha</label>
                                                        {/*<input type="text" className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} placeholder="YYYY/MM/DD" name={'startDate'} onChange={this.changeTextInput}/>*/}
                                                        <div>
                                                            <DatePicker selected={this.state.form.startDate} className={errors.startDate?"form-control datemask is-invalid":"form-control datemask"} 
                                                            onChange={date =>  {
                                                            this.changeDateInput('startDate',date)
                                                            }}
                                                        />
                                                        {errors.startDate?<div className="invalid-feedback" style={{display:'block'}}>{errors.startDate}</div> : null}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 d-flex justify-content-center">
                                                        <div className="author-box-center">
                                                            
                                                            {typefile=='image'?
                                                                <img alt="image" src="/dashboard/img/users/user-1.png"  id="previewimage" className="rounded-circle author-box-picture" style={{width: '100%',height:'500'}} />
                                                            :
                                                                <video className="product__item__pic set-bg " id="previewvideo"  controls>
                                                                    <source src={videouri}/>
                                                                </video>
                                                            }

                                                            
                                                            <div className="clearfix" />
                                                            <div className="author-box-name">
                                                            </div>
                                                            <div className="author-box-job d-flex justify-content-center">Vista Previa</div>
                                                        </div>
                                                    </div>


                                            </div>
                                            <button type="submit" className="btn btn-icon icon-left btn-primary"><i className="far fa-edit"></i> Publicar</button>
                                        </form>
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
        )
    }
}
export default withTracker((props)=>{
    const subcriptionCategory = Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    return {categorys,subcriptionCategory}
})(CreatePublication)

