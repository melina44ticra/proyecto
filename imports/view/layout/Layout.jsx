import React,{ Component } from 'react'
import { Tracker} from 'meteor/tracker'
import {Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Sidebar from '../../components/Sidebar'
import { Meteor } from 'meteor/meteor';
import Nav from '/imports/components/Nav'
import Home from '/imports/view/home/Home'
import { withTracker} from 'meteor/react-meteor-data'
import { param } from 'jquery'
import runGraphics from '/imports/assets/dashboard/js/page'
import "react-datepicker/dist/react-datepicker.css";
const feather = require('feather-icons')


class Layout extends Component {
    constructor(props){
        super(props)
        this.state={
            username:null,
            loader:true
        }
    }
        
    componentDidMount(){
        import '/imports/assets/dashboard/css'
        import '/imports/assets/dashboard/js'
        import dashboardDesingn from '/imports/assets/dashboard/js/scripts'
        
        
        dashboardDesingn()
        
    }
    componentDidUpdate(prevProps){
        if(this.props.username !== prevProps.username){
            console.log(this.props.username)
            runGraphics()
            feather.replace();
            setTimeout(function(){
                $(".loader").fadeOut("slow");
            },1000)
            
        }
    }
    render(){
        const {routes,username} = this.props
        return(
            <div>
                
                    <div className="loader"></div>
    
                    <div id="app">
                    <div className="main-wrapper main-wrapper-1">
                        <div className="navbar-bg"></div>
                        <Nav/>
                        <Sidebar/>
                        <div className="main-content">
                            <Switch>
                                {
                                routes.map((route,i)=>{
                                    return <SwitchRoutes key={i} {...route} />
                                })
                                }
                            </Switch>
                        </div>
                        
                        <footer className="main-footer">
                            <div className="footer-left">
                                <a href="templateshub.net">Templateshub</a>
                            </div>
                            <div className="footer-right">
                            </div>
                        </footer>
                    </div>
                </div>
           
                
             </div>
        ) 
        
    }
}

export default withTracker(params=>{
    return{
        username:Meteor.user()
    }
})(Layout)