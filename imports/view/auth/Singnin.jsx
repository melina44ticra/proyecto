import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import { Meteor } from 'meteor/meteor';

export default class Singnin extends Component {
    constructor(props){
        super(props)
        this.state={
            username:'',
            password:''
        }
    }
    SubmitForlogin =(e)=>{
        e.preventDefault()
        const mthis =this
        Meteor.loginWithPassword(this.state.username,this.state.password,function(error){
            if(error)
                console.log(error)
            else{
                //mthis.props.history.push('/dashboard/home')
                window.location.replace('/dashboard/home')
            }
                
        })

    }
    render() {
        return (
            <div> 
                <section className="normal-breadcrumb set-bg" data-setbg="/principal/img/normal-breadcrumb.jpg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <div className="normal__breadcrumb__text">
                                    <h2>Login</h2>
                                    <p>Welcome to the official Cartelera Movie.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="login spad">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="login__form">
                                    <h3>Login</h3>
                                    <form onSubmit={this.SubmitForlogin}>
                                        <div className="input__item">
                                            <input type="text" placeholder="Email address" onChange={e=> this.setState({username:e.target.value})}autocomplete="off" />
                                            <span className="icon_mail"></span>
                                        </div>
                                        <div className="input__item">
                                            <input type="password" placeholder="Password" onChange={e=> this.setState({password:e.target.value})}/>
                                            <span className="icon_lock"></span>
                                        </div>
                                        <button type="submit" className="site-btn">Iniciar Sesion</button>
                                    </form>
                                    <a href="#" className="forget_pass">Forgot Your Password?</a>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="login__register">
                                    <h3>Dont’t Have An Account?</h3>
                                    <a href="#" className="primary-btn">Register Now</a>
                                </div>
                            </div>
                        </div>
                        <div className="login__social">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-6">
                                    <div className="login__social__links">
                                        <span>or</span>
                                        <ul>
                                            <li><a href="#" className="facebook"><i className="fa fa-facebook"></i> Sign in With
                                                    Facebook</a></li>
                                            <li><a href="#" className="google"><i className="fa fa-google"></i> Sign in With
                                                    Google</a></li>
                                            <li><a href="#" className="twitter"><i className="fa fa-twitter"></i> Sign in With
                                                    Twitter</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <div className="search-model">
                    <div className="h-100 d-flex align-items-center justify-content-center">
                        <div className="search-close-switch"><i className="icon_close"></i></div>
                        <form className="search-model-form">
                            <input type="text" id="search-input" placeholder="Search here....." />
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}