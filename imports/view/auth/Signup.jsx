import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor';

export default class Signup extends Component {
    constructor(props){
        super(props)
        this.state={
            email:'',
            namecomplete:'',
            username:'',
            password:'',
            re_password:''
        }
    }
    SubmitForRegister = (e) =>{
        e.preventDefault()
        const form = {
            
            email:this.state.email,
            namecomplete:this.state.namecomplete,
            username:this.state.username,
            password:this.state.password,
            re_password:this.state.re_password 
        }
        Meteor.call('userCreate',form,function(error,resp){
            if(error){
                alert(error.reason)
            }else{
                alert(resp)
            }
        })

    }
    render() {
        return (
            <div>
                <section className="normal-breadcrumb set-bg" data-setbg="/principal/img/normal-breadcrumb.jpg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <div className="normal__breadcrumb__text">
                                    <h2>Registrate</h2>
                                    <p>Bienvenido a Cartelera</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="signup spad">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="login__form">
                                    <h3>Registrate Aqui</h3>
                                    <form onSubmit={this.SubmitForRegister}>
                                        <div className="input__item">
                                            <input type="text" placeholder="Correo Electronico" onChange={e=> this.setState({email:e.target.value})}  />
                                            <span className="icon_mail"></span>
                                        </div>
                                        <div className="input__item">
                                            <input type="text" placeholder="Nombre Completo" onChange={e=> this.setState({namecomplete:e.target.value})}  />
                                            <span className="icon_profile"></span>
                                        </div>
                                        <div className="input__item">
                                            <input type="text" placeholder="Nombre de usuario" onChange={e=> this.setState({username:e.target.value})}  />
                                            <span className="icon_profile"></span>
                                        </div>
                                        <div className="input__item">
                                            <input type="password" placeholder="Contraseña" onChange={e=> this.setState({password:e.target.value})}  />
                                            <span className="icon_lock"></span>
                                        </div>
                                        <div className="input__item">
                                            <input type="password" placeholder="Confirmar Contraseña" onChange={e=> this.setState({re_password:e.target.value})}  />
                                            <span className="icon_lock"></span>
                                        </div>
                                        <button type="submit" className="site-btn">Inicia ahora</button>
                                    </form>
                                    <h5>¿Ya tienes cuenta? <a href="#">Inicia!</a></h5>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="login__social__links">
                                    <h3>Inicia sesion con:</h3>
                                    <ul>
                                        <li><a href="#" className="facebook"><i className="fa fa-facebook"></i> Inicia con
                                                Facebook</a>
                                        </li>
                                        <li><a href="#" className="google"><i className="fa fa-google"></i> Inicia con
                                                Google</a></li>
                                        <li><a href="#" className="twitter"><i className="fa fa-twitter"></i> Inicia con
                                                Twitter</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}
